# Six obscure theme on 2 obscure window managers

* dk window manager and spectrwm
* hackthebox, moxer, oxocarbon, ramus, quixotic, nymph color schemes
* alactritty, dunst, fish, polybar, rofi, vscodium, gtk and icons themed

## Moxer

Official Theme Page: https://github.com/moxer-theme
<img src="assets/moxer.png">

## Hack the Box

Official Theme Page: https://github.com/silofy/hackthebox
<img src="assets/hackthebox.png">

## Nymph 

Official Theme Page: https://github.com/myagko/nymph
<img src="assets/nymph.png">

## Oxocarbon

Official Theme Page: https://github.com/nyoom-engineering/oxocarbon
<img src="assets/oxocarbon.png">

## Quixotic

Official Theme Page: https://github.com/QuixoticCS
<img src="assets/quixotic.png">

## Rasmus

Official Theme Page: https://github.com/kvrohit/rasmus.nvim
<img src="assets/rasmus.png">