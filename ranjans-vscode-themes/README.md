<div align="center">
  <img src="assets/icon.svg" alt="Logo" width="200p" height="112">
  <br/>
  <h3 align="center"A clean dark theme for vscode-oss based myagko's theme</h3>
</div>
 
<br/>
<p align="center">
  <img src="assets/sc1.png">
</p>

<p align="center">
  <img src="assets/sc2.png">
</p>

## Installation

* To install the extension copy it into the `<user home>/.vscode-oss/extensions` folder and restart Code.
* Open `File > Preferences > Color Themes` and pick nymph color theme.
