#!/bin/sh

function run {
 if ! pgrep $1 ;
  then
    $@&
  fi
}

run "lxsession"
run "udiskie"

#run "nm-applet" 
#run "volumeicon" 
#run "mate-power-manager" 

run "dunst"
run "picom"

pkill nitrogen 
nitrogen --restore &

wm=`wmctrl -m | grep "Name" | awk '{print $2}'`

if [ "$wm" == "dk" ]; then
	notify-send "logged into $wm"
  pkill sxhkd
  pkill polybar
  sxhkd -c "$HOME/.config/dk/sxhkdrc" &
  $HOME/.local/bin/polybar-launch.sh  
fi

if [ "$wm" == "spectrwm" ]; then
  notify-send "logged into $wm"
  pkill polybar
  $HOME/.local/bin/polybar-launch.sh  
fi

