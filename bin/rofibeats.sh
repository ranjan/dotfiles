#!/bin/sh

# add more args here according to preference
ARGS="--volume=60"

notification(){
# change the icon to whatever you want. Make sure your notification server 
# supports it and already configured.

# Now it will receive argument so the user can rename the radio title
# to whatever they want

	notify-send " Playing now: " "$@" --icon=media-tape
}

menu(){
	printf "1. Shripad Radio\n"
	printf "2. Nonstop Hindi\n"
	printf "3. Hindi Desi Bollywood Evergreen Hits\n"
	printf "4. Hindi Old Bollywood Songs\n"
	printf "5. Lyca Radio\n"
	printf "6. Exit"
}

main() {
	choice=$(menu | rofi -dmenu -p "Play" -font "Iosevka Term 13" | cut -d. -f1)

	case $choice in
		1)
			notification "  Shripad Radio ☕️🎶";
            URL="http://144.217.203.184:8112/stream"
			break
			;;
		2)
			notification "  Nonstop Hindi ☕️🎶";
            URL="http://s5.voscast.com:8216"
			break
			;;
		3)
			notification "  Hindi Desi Bollywood Evergreen Hits ☕️🎶";
            URL="http://104.167.2.55:8099/stream"
			break
			;;
		4)
			notification "  Hindi Old Bollywood Songs ☕️🎶";
            URL="http://listen.shoutcast.com/hindioldbollywoodsongs"
			break
			;;
		5)
			notification "  Lyca Radio ☕️🎶";
            URL="https://listen-lycaradio.sharp-stream.com/1458.mp3"
			break
			;;
		6)
                        notification "  Stop rofibeats ☕️🎶";
            pkill -f radio-mpv || main
                        break
                        ;;
	esac
    # run mpv with args and selected url
    # added title arg to make sure the pkill command kills only this instance of mpv
    mpv $ARGS --title="radio-mpv" $URL
}

pkill -f radio-mpv || main
