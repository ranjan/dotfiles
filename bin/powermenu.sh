#! /bin/sh

rofi_cmd() {
	rofi -dmenu \
		-theme ~/.config/rofi/powermenu.rasi
}

chosen=$(printf "󰗽\n\n󰐥\n󰒲" | rofi_cmd)

case "$chosen" in
	"󰗽") loginctl terminate-user `whoami` ;;
	"") systemctl reboot ;;
	"󰐥") systemctl poweroff ;;
	"󰒲") systemctl suspend ;;
	*) exit 1 ;;
esac
